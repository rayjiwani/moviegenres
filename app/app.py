# -*- coding: utf-8 -*-top250
import moviegenres
from flask import Flask
from flask import request
from flask import jsonify

print("\nImports Complete\n")

app = Flask(__name__)


def check_body(body, expected):
	if (not expected) or (type(expected) != dict):
		raise Exception("Invalid argument for 'expected'")

	if not body:
		return "body is empty"

	if type(body) != dict:
		return "body is not an object"

	missing = expected.keys() - body
	if len(missing):
		return "body is missing: " + ", ".join(missing)

	for field,dtype in expected.items():
		dtypes = dtype if(type(dtype) == list) else [dtype]
		if type(body[field]) not in dtypes:
			return "body has wrong type: " + field

	return ""
#def


# Returns {acc,f1}, where each field is an array of strings
# with the predicted genres for the model
@app.route('/predict', methods=['POST'])
def predict():
	# print("request.json:`", request.json)

	expected = {'plot': str}
	errmsg = check_body(request.json, expected)
	if errmsg:
		print(errmsg)
		return errmsg, 400

	try:
		prediction = moviegenres.predict(request.json['plot'])
		return prediction
	except:
		return "error making prediction", 500


# Returns [] of strings, containing all genres tested for
@app.route('/genres', methods=['POST'])
def genres():
	try:
		return jsonify(moviegenres.genres())
	except:
		return "error getting genres", 500
