# Need this for every B, if B includes C, and A in another folder includes B
import os as _os, sys as _sys; _sys.path.insert(0, _os.path.dirname(__file__))

import _mg

# List of genres predicted by the model
genres = list(_mg.genres.copy())

# Returns a dict with a key for each model ('acc', 'f1', etc)
# Each model contains an array of submodels, one for each genre
# eg: for some plot string p, model['acc'][0](p) 
# predicts plot p for genre genres[0] using model 'acc'
from mg_load import load_models

__all__ = ['genres', 'load_models']
