# Need this for every B, if B includes C, and A in another folder includes B
import os as _os, sys as _sys; _sys.path.insert(0, _os.path.dirname(__file__))

import numpy as np
import pandas as pd
pd.options.display.float_format = '{:,.2f}'.format

import _sk as sk


def absPath(path):
	import os
	return os.path.join(os.path.dirname(__file__), path)


# Generate these with the mg1/mg2/mg3 scripts
omdb = pd.read_json(absPath('./data/df_omdb.json.gz'), lines=True)
gdf = pd.read_json(absPath('./data/df_g.json'), lines=False)
genres = gdf['genre']


def splitData(df=omdb):
	X = df[['omdb_plot', 'omdb_genres']] 
	y = df[genres]
	X_train, X_test, y_train, y_test = sk.train_test_split(X, y)
	return X_train, X_test, y_train, y_test


def createModel(X, y):
	model = sk.make_pipeline(
		sk.CountVectorizer(),
		sk.TfidfTransformer(),
		sk.MultinomialNB()
	)
	model.fit(X, y)
	return model


def gdfSave():
	gdf.to_json(absPath('./data/df_g.json'), orient='records', lines=False)
