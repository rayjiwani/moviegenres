# Need this for every B, if B includes C, and A in another folder includes B
import os as _os, sys as _sys; _sys.path.insert(0, _os.path.dirname(__file__))

import _mg

genres = list(_mg.genres.copy())

from mg_load import load_models

__all__ = ['genres', 'load_models']
