# Need this for every B, if B includes C, and A in another folder includes B
import os as _os, sys as _sys; _sys.path.insert(0, _os.path.dirname(__file__))

from _mg import *

__all__ = ['load_models']


_mods = (
{
    'acc': {},
    'f1':  {},
})


# Use the processed omdb data and the previously calculated sampling ratios
# to quickly rebuild the models
def load_models():

    for modname,mod in _mods.items():
        mod['coefs'] = gdf[modname + '_coef']
        
    models = {modname: [None]*len(genres) 
              for modname in _mods}
    
    # Train on the full dataset since the params are finalized
    Xy_train = omdb
    
    for i in range(len(genres)):
        
        g = genres[i]
        print("%s [ " % (g), end='')
        is_g = Xy_train[ Xy_train[g] == 1 ]
        isnt_g = Xy_train[ Xy_train[g] == 0 ] 
        
        for modname,mod in _mods.items():
            sample_coef = mod['coefs'][i]
            print("%s(%.2f) " % (modname, sample_coef), end='')
            
            # Undersample movies that arent in the genre
            isnt_g_newlen = int(len(is_g) * sample_coef)
            isnt_g_undersampled = isnt_g.sample(n=isnt_g_newlen)
            Xy_train_u = pd.concat([is_g, isnt_g_undersampled])
            
            X_train_u = Xy_train_u['omdb_plot']
            y_train_u = Xy_train_u[g]
            model_u = createModel(X_train_u, y_train_u)
            
            models[modname][i] = model_u
        #for modname,mod in models.items
        print("]")
        
    #for i in range(len(genres))
    
    return models
    