# Need this for every B, if B includes C, and A in another folder includes B
import os as _os, sys as _sys; _sys.path.insert(0, _os.path.dirname(__file__))

import mg as _mg


_models = _mg.load_models()


def predict(plot):
    
    preds = {modelname: [] 
             for modelname in _models}
    
    for i in range(len(_mg.genres)):
        g = _mg.genres[i]
        #print( "Predicting genre " + g + "...")
        
        for modelname,model in _models.items():
            #print("\t" + modelname)
            if model[i].predict([plot]) == 1:
                preds[modelname].append(g)
        #for
    #for
    
    return preds


def genres():
    return _mg.genres.copy()
