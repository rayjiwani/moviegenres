sudo apt-get install python3.6 -y
sudo apt install python3-pip -y
sudo apt-get install python3-venv -y

# Note: you need to re-create the virtual environment if you change its dir

# Create virtual environment (second arg is folder name)
python3 -m venv venv
# Start virtual environment
source venv/bin/activate
# Exit virtual environment
deactivate
# Delete virtual environment
rm -rf venv

# Install dependencies manually
pip install numpy
pip install pandas
pip install scikit-learn
pip install Flask

# Save dependencies
pip freeze > requirements.txt

# Install dependencies from file
pip install -r requirements.txt
