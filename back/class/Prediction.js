'use strict';

function Prediction()
{
	this.conservative = [],
	this.aggressive = [],

	Object.seal(this);
	return this;
}

module.exports = Prediction;
