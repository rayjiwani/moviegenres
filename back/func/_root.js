'use strict';
const path = require('path');


const _ROOT = "./../";

function ROOT(relativepath)
{
	return path.join(__dirname, _ROOT, relativepath);
}


exports.ROOT = ROOT;
