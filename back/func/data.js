'use strict';
const {ROOT} = require('./_root.js');
const {freeze} = require(ROOT('./func/misc.js'));
const chalk = require('chalk');
const fs = require('fs');
const http = require('http');


function error(err, msg, fail)
{
	console.log(chalk.red(`ERROR: ${msg}`));
	console.log(err);

	if(fail instanceof Function)
		return fail(err);
}


// filepath		- absolute path
//
function readFileSync(filepath)
{
	try
	{
		return fs.readFileSync(filepath);
	}
	catch(err)
	{
		error(err, `readFileSync() - ${filepath}`);
		return false;
	}
}


// str 		- the json string to parse
//
function parseJSON(str)
{
	try
	{
		return JSON.parse(str);
	}
	catch(err)
	{
		error(err, `parseJson() - ${str}`)
		return false;
	}
}


// options	- {host, path, port, method, headers, ..}
//				(ie, nodejs http module request options)
// jsonStr 	- request body
// succ		- succ(data)
// fail		- fail(err)
//
function _httpRequest(options, jsonStr, succ, fail)
{
	const args = JSON.stringify({options, jsonStr}, null, 4);
	const request = http.request(options, function(response)
	{
		response.on('error', function(err)
		{
			return error(err, `httpRequest() response erorr - ${args}`, fail);
		});

		let body = "";
		response.on('data', function(chunk)
		{
			body += chunk;
		});

		response.on('end', function()
		{
			if(response.statusCode !==  200)
			{
				const e = new Error(response.statusCode + " - " + body);
				return error(e, `httpRequest() response end - ${args}`, fail);
			}

			let data = parseJSON(body);
			if(!data)
				return fail(null)

			console.log("httpRequest():", data);
			return succ(data);
		});
	});

	request.on('error', function(err)
	{
		return error(err, `httpRequest() request error - ${args}`, fail);
	});

	// Request body
	if(jsonStr)
	{
		request.write(jsonStr);
	}
	
	// This end() is called after the request is sent, not gauranteed to have a response yet
	request.end();
}

//	options		- {host, path, port}
//
function httpPost(options, jsonObj, succ, fail)
{
	const jsonStr = JSON.stringify(jsonObj);
	// MUST use this to get the correct length when strings contain special characters!
	const jsonStrLen = Buffer.byteLength(jsonStr);

	options.method = 'POST';
	options.headers = {};
	options.headers['Content-Type'] = 'application/json; charset=utf-8';
	options.headers['Content-Length'] = jsonStrLen;
	return _httpRequest(options, jsonStr, succ, fail);
}

//	options		- {host, path, port}
//
function httpGet(options, succ, fail)
{
	options.method = 'GET';
	return _httpRequest(options, null, succ, fail);
}



module.exports = freeze({error, httpGet, httpPost, parseJSON, readFileSync});
