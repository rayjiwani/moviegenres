'use strict';
const {ROOT} = require('./_root.js');
const {freeze} = require(ROOT('./func/misc.js'));


// Set the message field of err, set the response status, and call the global error handler
function error(req,res,next, err, message, status=500)
{
	err = err || new Error();
	err.message = message || "UNKNOWN ERROR";
	console.trace();
	res.status(status);
	// Need return here!
	return next(err);
}


module.exports = freeze({error});
