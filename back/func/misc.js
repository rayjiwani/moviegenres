'use strict';
const {ROOT} = require('./_root.js');



function isArray(x)
{
	return x instanceof Array;
}


// return:	[0,n)
//
function randomIndex(n)
{
	return Math.floor(Math.random() * n);
}

function randomElement(a)
{
	const i = randomIndex(a.length);
	return a[i];
}

function nextIndex(a, i, circular=false)
{
	if(i+1 < a.length)
		return i+1;

	else if(circular)
		return 0;

	else
		return i;
}

function prevIndex(a, i, circular=false)
{
	if(i-1 >= 0)
		return i-1;

	else if(circular)
		return a.length - 1;

	else
		return i;
}


// recursively call func on the objects and arrays contained in obj
// returns obj
//
function apply(obj, func, debug=0)
{
	function tabs(n)
	{
		s = "";
		for(let i=0; i<n; i++)
			s += "\t";

		return s;
	}

	if(isArray(obj))
	{
		obj.forEach(function(elem)
		{
			apply(elem, func, debug+1);
		});
		// console.log(tabs(debug), obj);
		func(obj);
	}
	else if(typeof obj === 'object')
	{
		for(let key in obj)
		{
			apply(obj[key], func, debug+1)
		}
		// console.log(tabs(debug), obj);
		func(obj);
	}
	else
	{
		
	}

	return obj;
}


function freeze(obj)
{
	return apply(obj, Object.freeze);
}



module.exports = freeze({apply, freeze, nextIndex, prevIndex, randomElement, randomIndex, isArray});
// This is so the script can copy it as a regular JS file
//# export {apply, freeze, nextIndex, prevIndex, randomElement, randomIndex, isArray};
