'use strict';
const {ROOT} = require('./_root.js');
const {httpPost, httpGet} = require(ROOT('./func/data.js'));
const {freeze} = require(ROOT('./func/misc.js'));

const MGP_PORT = 4000;
const MGP_HOST = "";
const MGP_PATH = "";
const MGP_PATH_PREDICT = MGP_PATH + "predict";
const MGP_PATH_GENRES = MGP_PATH + "genres";

function RequestOptions(path)
{
	this.host = MGP_HOST;
	this.port = MGP_PORT;
	this.path = path;
	return this;
};


const mgp =
{
	predict(body, succ, fail)
	{
		const requestOptions = new RequestOptions(MGP_PATH_PREDICT);
		return httpPost(requestOptions, body, succ, fail);
	},
	
	genres(succ, fail)
	{
		const requestOptions = new RequestOptions(MGP_PATH_GENRES);
		return httpPost(requestOptions, null, succ, fail);
	},
};


module.exports = freeze(mgp);
