'use strict';
const {ROOT} = require('./_root.js');
const mgp = require(ROOT('./module/mgp.js'));
const mvs = require(ROOT('./module/mvs.js'));
const Prediction = require(ROOT('./class/Prediction.js'));
const {freeze, randomElement, randomIndex} = require(ROOT('./func/misc.js'));
const {error} = require(ROOT('./func/data.js'))

const moviegenres =
{
	// Predict the genres of a movie
	// body = {plot}
	predict(body, succ, fail)
	{
		mgp.predict(body, function(data)
		{
			if(!data || !data.acc || !data.f1)
				return fail(new Error("failed to get predictions"));

			const prediction = new Prediction();
			prediction.conservative = data.acc;
			prediction.aggressive = data.f1;
			// console.log(prediction);
			return succ(prediction);
		},
		function(err)
		{
			return fail(err);
		});
	},

	// Get a random movie
	random()
	{
		return mvs.random();
	},

	// Get the list of genres
	genres(succ, fail)
	{
		mgp.genres(function(data)
		{
			if(!data || !data.length)
				return fail(new Error("failed to get genres"));
			
			return succ(data);
		},
		function(err)
		{
			return fail(err);
		});
	},
}


module.exports = freeze(moviegenres);
