'use strict';
const {ROOT} = require('./_root.js');
const {parseJSON, readFileSync} = require(ROOT('./func/data.js'));
const {freeze, randomElement, randomIndex} = require(ROOT('./func/misc.js'));

const _movies = freeze(parseJSON(readFileSync(ROOT('./data/movies.json'))));
console.log("_movies:", _movies.length);


const mvs =
{
	random()
	{
		return randomElement(_movies);
	},
};


module.exports = freeze(mvs);
