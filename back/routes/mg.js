'use strict';
const {ROOT} = require('./_root.js');
const moviegenres = require(ROOT('./module/moviegenres.js'));
const {error} = require(ROOT('./func/middleware.js'));
const router = require('express').Router();


// Predict the genres of a movie
// Request body: {plot: <String>}
router.post('/predict', function(req,res,next)
{
	if((!req.body) || (typeof req.body.plot !== 'string') || (!req.body.plot.trim().length))
	{
		return error(req,res,next, null, "Invalid request body for prediction", 400);
	}

	moviegenres.predict(req.body, function(prediction)
	{
		console.log()
		return res.json(prediction);
	}, 
	function(err)
	{
		return error(req,res,next, err, "Error making prediction", 500);
	});
});


// Get a random movie
router.post('/random', function(req,res,next)
{
	const movie = moviegenres.random();
	if(movie)
	{
		console.log()
		return res.json(movie);
	}
	else
	{
		return error(req,res,next, null, "Error getting random movie", 500);
	}
});


// Get the list of genres
router.post('/genres', function(req,res,next)
{
	moviegenres.genres(function(genres)
	{
		console.log();
		return res.json(genres);
	},
	function(err)
	{
		return error(req,res,next, err, "Error getting genres", 500);
	})
});


module.exports = router;
