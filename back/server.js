'use strict';
const config = require('./config.js');

// const cors = require('cors');
const express = require('express');
const app = express();


// app.use(cors())
app.use(express.json());
// app.use(express.urlencoded( {extended: false} ));

app.use('/', function(req,res,next)
{
	console.log(req.method, "request:", req.url, JSON.stringify(req.body));
	next();
});
app.use('/mg', require('./routes/mg'));
// app.use(express.static('./frontend'));
app.use(express.static('./static'));

app.use(function(err,req,res,next)
{
	console.log(`Server Error: ${res.statusCode} (${err.message})\n`);
	if(res.statusCode === 200) res.status(500);
	res.send(err.message);
});
app.use('*', function(req,res,next)
{
	res.sendStatus(404);
});


app.listen(config.port, function()
{
	console.log("Listening on", config.port);
});
