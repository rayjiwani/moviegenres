'use strict';
import {freeze, nextIndex, prevIndex, randomIndex} from '../func/misc.js'


class _Movie
{
	constructor(moviedata)
	{
		this._mv = freeze(moviedata || null);

		this._plotIdx = -1;
		this._numPlots = 0;
		
		if(this.hasPlot())
		{
			this._numPlots = this._mv.plots.length;
			this._plotIdx = randomIndex(this._numPlots);
		}

		return this;
	}
}

_Movie.prototype.hasPlot = function()
{
	// return !!this._mv?.plots?.length;
	return (this._mv) && (this._mv.plots) && (this._mv.plots.length);
}

_Movie.prototype.numPlots = function()
{
	return this._numPlots;
}

_Movie.prototype.currPlotNum = function()
{
	return this._plotIdx + 1;
}

_Movie.prototype.currPlot = function()
{
	return (this.hasPlot()) ? (this._mv.plots[this._plotIdx]) : ("No Plot Available")
}

_Movie.prototype.incPlot = function(circular=true)
{
	this._plotIdx = nextIndex(this._mv.plots, this._plotIdx, circular);
}

_Movie.prototype.decPlot = function(circular=true)
{
	this._plotIdx = prevIndex(this._mv.plots, this._plotIdx, circular);
}



class Movie extends _Movie
{
	constructor(moviedata)
	{
		super(moviedata);
		return this;
	}
}

Movie.prototype.getFullTitle = function()
{
	// const name = this._mv?.title || "UNKNOWN TITLE";
	// const year = this._mv?.year || "UNKNOWN YEAR";
	const name = (this._mv && this._mv.title) || "UNKNOWN TITLE";
	const year = (this._mv && this._mv.year) || "UNKNOWN YEAR";
	return `${name} (${year})`;
}

Movie.prototype.getGenresAsStr = function(sep=", ")
{
	// return this._mv?.genres?.join(sep) || "";
	return (this._mv && this._mv.genres && this._mv.genres.join(sep)) || ""
}


export {Movie};
