'use strict';

const MG_HOST = ""
const MG_PATH = "/mg/"
const MG_URL = MG_HOST + MG_PATH;
const MG_URL_PREDICT = MG_URL + "predict";
const MG_URL_RANDOM = MG_URL + "random";
const MG_URL_GENRES = MG_URL + "genres";


function ajax(url, body, succ, fail)
{
	// function ajaxUrlencoded(url, body, succ, fail)
	// {
	// 	$.ajax(
	// 	{
	// 		method: 		'post',
	// 		url: 			url,
	// 		data: 			body || "",
	// 		success: 		succ,
	// 		error: 			fail,
	// 	});
	// }

	function ajaxJson(url, body, succ, fail)
	{
		$.ajax(
		{
			method: 		'post',
			url: 			url,
			// Send as JSON vs UrlEncoded. Must set contentType.
			// If using JSON, empty body must be {} not "".
			data: 			JSON.stringify(body || {}),
			contentType: 	'application/json',
			success: 		succ,
			error: 			fail,
		});
	}

	return ajaxJson(url, body, succ, fail);
}


const moviegenres =
{
	predict(plot, succ, fail)
	{
		return ajax(MG_URL_PREDICT, {plot}, succ, fail);
	},

	random(succ, fail)
	{
		return ajax(MG_URL_RANDOM, "", succ, fail);
	},

	genres(succ, fail)
	{
		return ajax(MG_URL_GENRES, "", succ, fail);
	},
}


export {moviegenres};
