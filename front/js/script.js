'use strict'
import {Movie} from './class/Movie.js';
import {moviegenres} from './module/moviegenres.js';
import {isArray} from './func/misc.js';


const HASH_HOME = "#home";
const HASH_INFO = "#info";
const HASH_ABOUT = "#about"

const _content = {};
_content[HASH_HOME] = document.getElementById("content-home");
_content[HASH_ABOUT] = document.getElementById("content-about");
_content[HASH_INFO] = document.getElementById("content-info");
// Note: This only applies to the top-level object; can still work with the elements
Object.freeze(_content);

const _nav = {};
_nav[HASH_HOME] = document.getElementById("nav-home");
_nav[HASH_ABOUT] = document.getElementById("nav-about");
_nav[HASH_INFO] = document.getElementById("nav-info");
Object.freeze(_nav);

const _title = document.getElementById("title");

const _btnClear = document.getElementById("btn-clear");
const _btnRandom = document.getElementById("btn-random");
const _lnkRestore = document.getElementById("lnk-restore");
const _lnkPrevplot = document.getElementById("lnk-prevplot");
const _lnkNextplot = document.getElementById("lnk-nextplot");

const _inputPlot = document.getElementById("input-plot");
const _outputResults = document.getElementById("output-results");
const _tblResults = document.getElementById("tbl-results");
const _resultsConservative = document.getElementById("results-conservative");
const _resultsAggressive = document.getElementById("results-aggressive");
const _errResults = document.getElementById("err-results");

const _outputMovieinfo = document.getElementById("output-movieinfo");
const _movieinfoTitle = document.getElementById("movieinfo-title");
const _movieinfoActualgenres = document.getElementById("movieinfo-actualgenres");
const _movieinfoPlotnum = document.getElementById("movieinfo-plotnum");
const _movieinfoNumplots = document.getElementById("movieinfo-numplots");
const _errMovieinfo = document.getElementById("err-movieinfo")

const _outputGenres = document.getElementById("output-genres");
const _lstGenres = document.getElementById("lst-genres")
const _errGenres = document.getElementById("err-genres");


const style = { color: {} }
style.colorActiveNav = 'white';
style.colorInactiveNav = 'black';
style.colorMovieinfoUneditedPlot = 'black';
style.colorMovieinfoEditedPlot = 'grey';
style.colorActiveNavBg = '#173148';
style.colorInactiveNavBg = '#366b99';
Object.seal(style);

const HOVER_NAVIGATION = true;
const HIGHLIGHT_CURRENT_NAV = 0
const HIGHLIGHT_CURRENT_NAV_BG = 1

// ----------------------------------------------------------------------------

const mg = {};
mg.init = function()
{
	this.movie = 		null;
	this.pred = 		null;
	this.showMovie = 	false;
	this.plotIsEdited = false;
	this.genres =		null;
	Object.seal(this);
}

document.addEventListener('DOMContentLoaded', function()
{
	ajax_genres();
	mg.init(); 
	setPlotInput(""); 
	displayContent();
	console.log("OK")
});

window.onhashchange = function()
{ 
	displayContent(); 
};

// ----------------------------------------------------------------------------

// move to seperate file:

function invisible(elem)
{
	elem.style.visibility  = 'hidden';
}

function visible(elem)
{
	elem.style.visibility  = 'visible';

}

function hide(elem)
{
	elem.style.display = 'none';
}

function show(elem, display)
{
	elem.style.display = display || 'block';
}

function showText(elem, text, hideElem, display)
{
	if(hideElem)
	{
		hide(hideElem)
	}
	elem.textContent = text;
	show(elem, display);
}

function inputIsScrollable(inputElem)
{
	return (inputElem.scrollHeight > inputElem.clientHeight);
}

function inputFocusOnStart(inputElem)
{
	inputElem.setSelectionRange(0,0);
}


// ----------------------------------------------------------------------------

function displayContent()
{
	// Figure out what to display
	window.location.hash = window.location.hash.toLowerCase();
	if(_content[window.location.hash] === undefined)
	{
		window.location.hash = "";
	}
	const hash = window.location.hash || HASH_HOME

	// Hide everything except the content being viewed
	for(const h in _content)
	{
		hide(_content[h]);
		if(HIGHLIGHT_CURRENT_NAV)
		{
			_nav[h].style.color = style.colorInactiveNav;
		}
		if(HIGHLIGHT_CURRENT_NAV_BG )
		{
			_nav[h].style.backgroundColor = style.colorInactiveNavBg;
		}
	}

	show(_content[hash]);
	if(HIGHLIGHT_CURRENT_NAV)
	{
		_nav[hash].style.color = style.colorActiveNav;
	}
	if(HIGHLIGHT_CURRENT_NAV_BG )
	{
		_nav[hash].style.backgroundColor = style.colorActiveNavBg;
	}

	// Configure the state for non-Home content

	// Configure the state for Home
	if(hash === HASH_HOME)
	{
		// Set focus, etc
		afterPlotChange();
	}
}


if(HOVER_NAVIGATION)
{
	for(let h in _nav)
	{
		_nav[h].addEventListener('mouseover', function()
		{
			window.location.hash = h;
		})
	}
}

_title.addEventListener('click', function()
{
	window.location.href = "/";
});

// ----------------------------------------------------------------------------

// #info

function setGenresOutput(genresArr)
{
	// Note: do not write to _outputGenres.textContent, it will erase the inner list
	// Create an inner element if you need text
	show(_outputGenres);
	hide(_errGenres);

	let html = "";
	genresArr.forEach(function(genre)
	{
		html += `<li>${genre}</li>`;
	});
	_lstGenres.innerHTML = html;
}

function setGenresError(errmsg)
{
	showText(_errGenres, errmsg||"unable to get genres", _outputGenres);
}

// ----------------------------------------------------------------------------

// #home

function updateMovieOutput()
{
	hide(_errMovieinfo);
	show(_outputMovieinfo);

	visible(_outputMovieinfo);

	if(!mg.showMovie)
	{
		invisible(_outputMovieinfo);
		return;
	}

	else if(mg.plotIsEdited)
	{
		_outputMovieinfo.style.color = style.colorMovieinfoEditedPlot
		visible(_outputMovieinfo);
		show(_lnkRestore);
		return;
	}

	// (mg.showMovie && !mg.plotEdited)
	_outputMovieinfo.style.color = style.colorMovieinfoUneditedPlot
	_movieinfoTitle.textContent = mg.movie.getFullTitle();
	_movieinfoActualgenres.textContent = mg.movie.getGenresAsStr();
	_movieinfoPlotnum.textContent = mg.movie.currPlotNum();
	_movieinfoNumplots.textContent = mg.movie.numPlots();
	// show(_outputMovieinfo);
	visible(_outputMovieinfo);
	hide(_lnkRestore);

	// New movies with long plots should have the cursor at the start
	if(inputIsScrollable(_inputPlot))
	{
		inputFocusOnStart(_inputPlot);
	}
}

function setMovieError(errmsg)
{
	showText(_errMovieinfo, errmsg||"unable to get movie", _outputMovieinfo);
}

// Pass in null to hide results
function setPredOutput(predObj)
{
	hide(_errResults);
	show(_outputResults);

	visible(_outputResults);

	if(!predObj)
	{
		invisible(_outputResults);
		return;
	}

	_resultsConservative.textContent = predObj.conservative.join(", ") || "(none)";
	_resultsAggressive.textContent = predObj.aggressive.join(", ") || "(none)";
}

function setPredError(errmsg)
{
	console.log(errmsg)
	showText(_errResults, errmsg||"unable to make prediction", _outputResults);
}

// This does not modify app.plotIsEdited; do that before calling this function
function afterPlotChange()
{
	// The text box has has been cleared manually or via the button
	if(!_inputPlot.value.trim())
	{
		// Clear whitespace to show the placeholder
		_inputPlot.value = ""
		
		mg.showMovie = false;
		setPredOutput(null);
	}
	else
	{
		ajax_predict();
	}

	updateMovieOutput();
	_inputPlot.focus();
}

function setPlotInput(plotStr)
{
	_inputPlot.value = plotStr || "";

	// -- NOTE THIS FOR REFERENCE
	// Changing the value like this doesn't trigger the event, manually do it:
	// input_plot.dispatchEvent(new Event('input'));

	afterPlotChange();
}

function resetPlot()
{
	mg.plotIsEdited = false;
	setPlotInput(mg.movie.currPlot());
}

// ----------------------------------------------------------------------------

// #home

_btnClear.addEventListener('click', function()
{ 
	setPlotInput(""); 
});

_btnRandom.addEventListener('click', function()
{ 
	ajax_random(); 
});

_inputPlot.addEventListener('input', function()
{
	mg.plotIsEdited = true;
	afterPlotChange();
});

_lnkRestore.addEventListener('click', function()
{
	resetPlot();
});

_lnkPrevplot.addEventListener('click', function()
{
	mg.movie.decPlot();
	resetPlot();
});

_lnkNextplot.addEventListener('click', function()
{
	mg.movie.incPlot();
	resetPlot();
});

// ----------------------------------------------------------------------------

function ajax_predict(plot)
{
	moviegenres.predict(_inputPlot.value, function(pred)
	{
		mg.pred = pred;
		setPredOutput(mg.pred);
	},
	function(err)
	{
		setPredError(err.responseText);
	});
}

function ajax_random()
{
	moviegenres.random(function(moviedata)
	{
		mg.movie = new Movie(moviedata);
		mg.showMovie = true;
		resetPlot();
	},
	function(err)
	{
		setMovieError(err.responseText);
		// setPredOutput(null);
	});
}

function ajax_genres()
{
	moviegenres.genres(function(genres)
	{
		mg.genres = genres;
		setGenresOutput(mg.genres);
	},
	function(err)
	{
		setGenresError(err.responseText);
	});
}

// ----------------------------------------------------------------------------
