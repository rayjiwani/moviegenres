#
sudo apt-get install unzip -y
sudo apt install curl -y

# entr
sudo apt install entr -y

# Git
sudo apt install git -y

# NodeJS
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs -y
# PM2
sudo npm install pm2 -g

# Python3
sudo add-apt-repository ppa:deadsnakes/ppa -y \
&& sudo apt update -y \
&& sudo apt install python3.6 -y
sudo rm /usr/bin/python3
sudo ln -s /usr/bin/python3.6 /usr/bin/python3
sudo apt install python3-pip -y
sudo python3 -m pip install --upgrade pip
sudo apt-get install python3-venv -y

pip3 install --upgrade pip
