bakdate=`date +%m%d`
# bakdate=${bakdate:1}

alphabet="abcdefghijklmnopqrstuvwxyz"
i=0
letter=${alphabet:i:1}

filename="mg."${bakdate}${letter}".zip"
filepath="./x/"


while test -e "${filepath}${filename}";
do
	(( i++ ))
	letter=${alphabet:i:1}
	filename="mg."${bakdate}${letter}".zip"
done
echo ${filepath}${filename}


mkdir -p ${filepath}
zip -r ${filepath}${filename} . -x ./app/venv/\* *__pycache__*/\* *node_modules*/\* ./x/\*
echo ${filepath}${filename}
xdg-open ${filepath}
