#!/bin/bash

BACK_PORT=3000
APP_PORT=4000

copy_js_file() {
	# Copy the NodeJS file for use with JS
	cp back/func/misc.js .
	# Delete NodeJS import and export statements 
	sed -i '/^const {/d' misc.js
	sed -i '/^module.exports/d' misc.js
	# Uncomment any //# to expose the JS export statement
	sed -i 's/\/\/#//' misc.js
	mv misc.js front/js/func/misc.js
}


deploy=0
deploy=${1:-$deploy}


copy_js_file
cd back
rm -rf "$(pwd)/static"
ln -s "$(pwd)/../front" "$(pwd)/static"
# $BACK_INSTALL_CMD
npm install
if (( $deploy == 0 ));
then
	gnome-terminal -x sh -c "find . ../front -iname '*.js' -o -iname '*.html' -o -iname '*.css' -o -iname '*.json' -not -path './node_modules/*' | PORT=$BACK_PORT entr -rc node server.js"
else
	sudo iptables -A PREROUTING -t nat -p tcp --dport 80 -j REDIRECT --to-port $BACK_PORT
	pm2 delete 'sudo node server'
	pm2 start 'sudo node server'
fi
cd ..

cd app
if [[ ! -d "$(pwd)/venv/bin/activate" ]]; then python3 -m venv venv; fi
source "$(pwd)/venv/bin/activate"
pip3 install -r requirements.txt
if (( $deploy == 0 ));
then
	gnome-terminal -x sh -c "FLASK_ENV=development ./start_app.sh $APP_PORT"

else
	pm2 delete start_app;
	pm2 start 'FLASK_ENV=production ./start_app.sh $APP_PORT'
fi
cd ..

if (( $deploy == 0 ));
then
	firefox "localhost:$BACK_PORT" &
	clear
fi
